import express from "express"; // import API library
import { connect } from "mongoose"; // import DB library
import "core-js/stable"; // import JS core
import "regenerator-runtime/runtime"; // import transpiler
import funkbook_users from "./models/funkbook_users"; // import users model from DB
let jwt = require("jsonwebtoken");//todo move to auth
const bcrypt = require('bcrypt');//todo move to auth
let auth = require("./auth");

const app = express(); // create API object and name it app

// Connect to correct DB
connect("mongodb://localhost:27017/funkbook").then((res) => {
  console.log("DB Connected");
});

// Create JWT secret in RAM // IRL it should be an environment variable
let SecretEnvironmentVariable = "62MZh&6U?YdSs#Nt";
let defaultTokenExpiry = "2m"

// Initiate API object
app.listen(80, () => {
  console.log("server started");
});

// Home Page
app.get("/", (req, res) => {
  return res.status(200).send("Welcome to the funkbook home page. Please SignUp or Login to get past here");
});

// Sign Up API
app.get("/signUp", async (req, res) => {
  try {
    // check if email id exists in db and mark with a response
    let userData = await funkbook_users.find({ email: req.query.email });
    if (userData.length > 0) {
      return res.status(400).json("Error:Email ID already exists. Code 1001"); //error
    }
    // check if username exists in db and mark with a response
    userData = await funkbook_users.find({ username: req.query.username });
    if (userData.length > 0) {
      return res.status(400).json("Error:username already taken. Code 1002"); //error
    }
  } catch (myErr) { // catch any random errors and handle without crashing the API app
    console.log(myErr.message);
    return res.status(500).json("server error! check logs pls"); //error
  }
  // user is new, so create a new user in DB
  try {
    let signupForm = req.query;
    let saltRounds = 10;
    // hash the PlainText password & delete it
    bcrypt.hash(signupForm.password, saltRounds, async function (err, hash) {
      delete signupForm.password;
      signupForm.passwordHash = hash;
      // store hash in DB
      const user = new funkbook_users(signupForm);
      let userData = await user.save();
      delete userData.passwordHash;//todo why is this not working? Await is not blocking??
      return res.status(200).json(userData);
    });
  } catch (myErr) { // catch any random errors and handle without crashing the API app
    console.log(myErr.message);
    return res.status(500).json("server error! check logs pls"); //error
  }
});

//Login API
app.get("/login", async (req, res) => {
  let loginForm = req.query
  let userData = {}
  try {
    // check if username exists in db and mark with a response
    userData = await funkbook_users.findOne({username: loginForm.username});
    if (userData == null || userData == undefined) {
      return res.status(403).json("Error: username or password error. Code 1003"); //username error
    }
    // compare plaintext & DB hash using bcrypt
    bcrypt.compare(loginForm.password,userData.passwordHash, function (err, result) {
      if (result != true) {
        return res.status(403).json("Error: username or password error. Code 1004"); //password error
      } else {
        //  create a JWT token with payload as user id (findbyid is faster for DB)
        let newToken = jwt.sign(
          { userID: userData.id },
          SecretEnvironmentVariable,
          { expiresIn: defaultTokenExpiry }
        );
        return res.status(200).json({token:newToken}); //login success
      }
    });
  } catch (myErr) { // catch any random errors and handle without crashing the API app
    console.log(myErr.message);
    return res.status(500).json("server error! check logs pls"); //error
  }
});

//return any user details
app.get("/getUserByUserName",auth.validate ,async (req, res) => {
  try {
    // verify token and work with DB\
    if(!req.sessionIsValid)
      return;
    let userData = await funkbook_users.findOne({username:req.query.username});
    if(userData == null || userData == undefined){
      return res.status(200).json({msg:"No user with this username found!"});
    }else{
      delete userData.passwordHash;// todo why is this not working? timing/async issue?
      return res.status(200).json(userData);
    }
  } catch (myErr) {
    console.log(myErr.message);
    return res.status(500).json("server error! check logs pls"); //error
  }
});

// modify email todo next
app.get("/modifyEmailID", async (req, res) => {
  try {
    const userData = await SCHEMA_NAME.findOne(req.query.id);
    userData.email = "This is the new email";
    let updated_user_data = new SCHEMA_NAME(userData);
    await Telkin_User.findOneAndUpdate(req.query.id, updated_user_data);
    return res.status(200).json(userData);
  } catch (myErr) {
    return res.status(500).json(myErr.message); //error
  }
});
