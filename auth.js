// auth.js
// ========
let SecretEnvironmentVariable = "62MZh&6U?YdSs#Nt";
let jwt = require("jsonwebtoken");
import funkbook_users from "./models/funkbook_users"; // import users model from DB

module.exports = {
  validate: async function (req,res,next) {
    let sessionIsValid = false;
    // console.log(`checking req data: ${JSON.stringify(req, null, 4)}\n`)
    // console.log(`checking res data: ${JSON.stringify(res, null, 4)}\n`)
    let decoded = "";
    try {
      try {
        decoded = jwt.verify(req.query.token, SecretEnvironmentVariable);
      } catch (err) {
        if (err.message == "jwt expired") {// token expired
          return res.status(401).json("Oops. Login session expired, please login again. Code 1005");//token expired
        } else {// token not decodable
          console.log(`Error:${err.message}`)
          return res.status(403).json("Oops. Session issue, please try to login again. Code 1006");//token issue
        }
      }
      try {
        let userMakingRequest = await funkbook_users.findById(decoded.userID);// find userMakingRequest
        if(userMakingRequest == null){
          throw "1007";
        }
      } catch (err) {// if userMakingRequest doesnt exist,
        console.log(`Error:${err.message}`)  
        return res.status(403).json("Oops. Session issue, please try to login again. Code 1007");//token ok but user deactivated
      }
    } catch (myErr) {
      console.log(myErr.message); //error
      return res.status(503).json("Oops. Server issue, please try to login again. Code 1008");//token ok but user deactivated
    }
    req.sessionIsValid = true;
    next();
  },
  renew: async function (sessionToken) {
    console.log("sessionToken=");
    console.log(sessionToken);
  },
};
