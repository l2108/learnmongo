import { Schema, model } from "mongoose";

const funkbook_users = new Schema({
  username: { type: String, required: true },
  name: { type: String, required: true },
  email: { type: String, required: true },
  passwordHash: { type: String, default: "1234" },
});

// export let fbu = model("User", funkbook_users);// but call it by import {fbu}
export default model("User", funkbook_users);